# vue-springboot

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


#### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Add Page

![Add Page](img/add.png "Add Page")

Search Page

![Search Page](img/search.png "Search Page")
